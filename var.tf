variable "tags" {
  type = map
  default = {
   "Project" = "VCC"
    "Environment" = "Development"
  }
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "248331077409"
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::248331077409:user/terraformcloud"
      username = "EC2Admin"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::248331077409:user/reginald.agbebaku"
      username = "reginald.agbebaku"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::248331077409:user/eks-terraform"
      username = "eks-terraform"
      groups   = ["system:masters"]
    },
  ]
}
