data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
	source                 	= "terraform-aws-modules/vpc/aws"
    version                	= "2.24.0"   
    name                   	= "VCC-DEV-VPC"    
    cidr                  	= "10.52.132.0/23"
	  azs                   = slice(data.aws_availability_zones.available.names, 0, 2) #--this is the AZ namesf
    public_subnets         	= ["10.52.132.64/26","10.52.133.64/26"]
    private_subnets        	= ["10.52.132.0/26","10.52.133.0/26","10.52.132.128/26","10.52.133.128/26","10.52.132.192/26","10.52.133.192/26"]
    enable_nat_gateway     	= true
    single_nat_gateway    	= false
    reuse_nat_ips  			= true
    external_nat_ip_ids 	= "${aws_eip.nat.*.id}"   # <= IPs specified here as input to the module
    one_nat_gateway_per_az	= true 
	 

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = "1"
  }
  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
  }
  tags = merge(
    var.tags,
    {
      "kubernetes.io/cluster/eks-test" = "shared"
    },
  )
}

resource "aws_eip" "nat" {
	count 	= 2	
	vpc 	= true
}



module "vote_service_sg" {
    source                                                   = "terraform-aws-modules/security-group/aws"
    version                                                 = "3.0.0"
    name                                                     = "user-service"
    description                                         = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
    vpc_id                                                   = module.vpc.vpc_id
    ingress_cidr_blocks                             = ["10.52.132.64/26","10.52.133.64/26"] #-- public subnet
    ingress_rules                                   = ["http-80-tcp",]
    ingress_with_cidr_blocks = [
      {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
    egress_rules                                     = ["all-all"]
}


variable "instances_number" {
  default = 1
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn2-ami-hvm-2.0.20200304.0-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "sgrdsec2"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}

module "ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"

  instance_count = var.instances_number

  name                        = "ec2-harbor-rds"
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = "t2.micro"
  subnet_id                   = module.vpc.private_subnets[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  associate_public_ip_address = false
  user_data = <<COMMANDS
    #!/bin/bash
    sudo yum install -y postgresql-server postgresql-contrib
    sudo postgresql-setup initdb
    systemctl start postgresql
    systemctl enable postgresql
    export PGPASSWORD=${module.aws_db_instance.this_db_instance_password}
    psql -h ${module.aws_db_instance.this_db_instance_address} -U ${module.aws_db_instance.this_db_instance_username} -d ${module.aws_db_instance.this_db_instance_name} << EOT
        CREATE DATABASE harbor_clair; 
        CREATE DATABASE harbor_notary_server; 
        CREATE DATABASE harbor_notary_signer;
    EOT
  COMMANDS
}

resource "aws_volume_attachment" "this_ec2" {
  count = var.instances_number

  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.this[count.index].id
  instance_id = module.ec2.id[count.index]
}

resource "aws_ebs_volume" "this" {
  count = var.instances_number

  availability_zone = module.ec2.availability_zone[count.index]
  size              = 1
}
###############KMS

resource "aws_kms_key" "harborEncryptRDS" {
  description         = "Key used to encrypt RDS Harbor"
  enable_key_rotation = true
}

resource "aws_kms_alias" "harborEncryptRDS" {
  name          = "alias/harborEncryptRDS"
  target_key_id = aws_kms_key.harborEncryptRDS.key_id
}

######RDS

resource "aws_security_group" "rds" {
  name                              = "harbor-rds-sg"
  description                       = "RDS postgres servers (terraform-managed)"
  vpc_id                            = module.vpc.vpc_id

  ingress {
    from_port                       = 5432
    to_port                         = 5432
    protocol                        = "tcp"
    cidr_blocks                     = module.vpc.private_subnets
  }

  egress {
    from_port                       = 0
    to_port                         = 0
    protocol                        = "-1"
    cidr_blocks                     = ["0.0.0.0/0"]
  }
}

module "aws_db_instance" {
    source                          = "terraform-aws-modules/rds/aws"
    engine                          = "postgres"
    engine_version                  = "9.6.14"
    identifier                      = "harbordb-rds"
    username                        = "harbor_admin"
    password                        = "Yg!84T%HE6@bGhOdN"
    family                          = "postgres9.6"
    instance_class                  = "db.t3.medium"
    storage_type                    = "gp2"
    allocated_storage               = 250
    max_allocated_storage           = 2000
    multi_az                        = false
    publicly_accessible             = false
    vpc_security_group_ids          = ["${aws_security_group.rds.id}", "${module.vote_service_sg.this_security_group_id}", "${module.security_group.this_security_group_id}"]
    subnet_ids                      = module.vpc.private_subnets
    create_db_subnet_group          = true
    port                            = "5432"
    name                            = "harbor_core"
    create_db_parameter_group       = false
    create_db_option_group          = false
    backup_retention_period         = 35
    backup_window                   = "03:00-06:00"
    storage_encrypted               = false
    kms_key_id                      = aws_kms_key.harborEncryptRDS.key_id
    create_monitoring_role          = false
    maintenance_window              = "Mon:00:00-Mon:03:00"
    enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
    deletion_protection             = false
    tags                            = {
                                        Owner       = "user"
                                        Environment = "dev"
                                      }
}


module "my-cluster2" {
  source                = "terraform-aws-modules/eks/aws"
  cluster_name          = "my-cluster"
  cluster_version       = "1.14"
  vpc_id                = "${module.vpc.vpc_id}"
  subnets               = ["${module.vpc.private_subnets[1]}", "${module.vpc.private_subnets[0]}", "${module.vpc.private_subnets[2]}"]
  cluster_security_group_id    = "${module.vote_service_sg.this_security_group_id}"

  worker_groups = [
    {
      instance_type                            = "t2.medium"
      asg_desired_capacity                     = "2"
      asg_min_size                             = "2"
      asg_max_size                             = "3"
    }
  ]

  map_roles                            = var.map_roles
  map_users                            = var.map_users
  map_accounts                         = var.map_accounts

}

data "aws_eks_cluster" "cluster" {
  name = module.my-cluster2.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.my-cluster2.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.10"
}



resource "aws_ecr_repository" "testecr" {
  name                 = "testecr"
  image_tag_mutability = "IMMUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "testecrpolicy" {
  repository = aws_ecr_repository.testecr.name

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        },
	{
            "rulePriority": 2,
            "description": "Keep last 30 images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["v"],
                "countType": "imageCountMoreThan",
                "countNumber": 30
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
